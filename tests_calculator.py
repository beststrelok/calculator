import unittest
from calculator import generate_token
from calculator import convert_to_RPN
from calculator import count_RPN
from calculator import calculate

class Test(unittest.TestCase):
    def test_generate_token(self):
        self.assertSequenceEqual(tuple(generate_token('5*2/3')), (5, '*', 2, '/', 3))
        self.assertSequenceEqual(tuple(generate_token('5/2*3')), (5, '/', 2, '*', 3))
        self.assertSequenceEqual(tuple(generate_token('5/5/5')), (5, '/', 5, '/', 5))
        self.assertSequenceEqual(tuple(generate_token('5*5*5')), (5, '*', 5, '*', 5))
        
        self.assertSequenceEqual(tuple(generate_token('5*2+12/6')), (5, '*', 2, '+', 12, '/', 6))
        self.assertSequenceEqual(tuple(generate_token('5+2^3')), (5, '+', 2, '^', 3))
        self.assertSequenceEqual(tuple(generate_token('1^2+6*4+3')), (1, '^', 2, '+', 6, '*', 4, '+', 3))

    def test_convert_to_RPN(self):
        self.assertEqual(convert_to_RPN(generate_token('5*2/3')), [5.0, 2.0, '*', 3.0, '/'])
        self.assertEqual(convert_to_RPN(generate_token('5/2*3')), [5.0, 2.0, '/', 3.0, '*'])
        self.assertEqual(convert_to_RPN(generate_token('5/5/5')), [5.0, 5.0, '/', 5.0, '/'])
        self.assertEqual(convert_to_RPN(generate_token('5*5*5')), [5.0, 5.0, '*', 5.0, '*'])

        self.assertEqual(convert_to_RPN(generate_token('5*2+12/6')), [5.0, 2.0, '*', 12.0, 6.0, '/', '+'])
        self.assertEqual(convert_to_RPN(generate_token('5+2^3')), [5.0, 2.0, 3.0, '^', '+'])
        self.assertEqual(convert_to_RPN(generate_token('1^2+6*4+3')), [1.0, 2.0, '^', 6.0, 4.0, '*', 3.0, '+', '+'])


    def test_count_RPN(self):
        self.assertEqual(count_RPN([5.0, 2.0, '*', 3.0, '/']), 3.3333333333333335)
        self.assertEqual(count_RPN([5.0, 2.0, '/', 3.0, '*']), 7.5)
        self.assertEqual(count_RPN([5.0, 5.0, '/', 5.0, '/']), 0.2)
        self.assertEqual(count_RPN([5.0, 5.0, '*', 5.0, '*']), 125)

        self.assertEqual(count_RPN([5.0, 2.0, '*', 12.0, 6.0, '/', '+']), 12)
        self.assertEqual(count_RPN([5.0, 2.0, 3.0, '^', '+']), 13)
        self.assertEqual(count_RPN([1.0, 2.0, '^', 6.0, 4.0, '*', 3.0, '+', '+']), 28)

    def test_calculate(self):
        self.assertEqual(calculate('5*2/3'), 3.3333333333333335)
        self.assertEqual(calculate('5/2*3'), 7.5)
        self.assertEqual(calculate('5/5/5'), 0.2)
        self.assertEqual(calculate('5*5*5'), 125)
        
        self.assertEqual(calculate('5*2+12/6'), 12)
        self.assertEqual(calculate('5+2^3'), 13)
        self.assertEqual(calculate('1^2+6*4+3'), 28)

if __name__ == "__main__":
    unittest.main()