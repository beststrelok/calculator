##################################
###       Calculator v1.9      ###
### on Reverse Polish Notation ###
##################################

#!/usr/bin/env python
from sys import exit as exit_calculator

# operators 
operators = {
    '^' : {
            "priority": 3,
            "action": lambda x,y: x**y,
        },
    '/' : {
            "priority": 2,
            "action": lambda x,y: x/y,
        },
    '*' : {
            "priority": 2,
            "action": lambda x,y: x*y,
        },
    '-' : {
            "priority": 1,
            "action": lambda x,y: x-y,
        },
    '+' : {
            "priority": 1,
            "action": lambda x,y: x+y,
        },
}

# generator
def generate_token(expression):
    string = expression.replace(' ','')
    chunk = ''
    signs = [n for n in operators]
    for char in string:
        if char not in signs:
            chunk += char
        else:
            yield float(chunk)
            yield char
            chunk = ''

    yield float(chunk)


def convert_to_RPN(pieces):
    out = []
    stack = []

    #shunting-yard algorithm
    for piece in pieces:
        if isinstance(piece,float):
            out.append(piece)
        else:
            op1 = piece
            if len(stack) > 0:
                op2 = stack[-1]
                if operators[op1]['priority'] <= operators[op2]['priority']:
                    op2 = stack.pop()
                    out.append(op2)
            stack.append(op1)
    return out + stack[::-1]

        
def count_RPN(rpn):
    answer = []

    # calculation on rpn
    for item in rpn:
        if isinstance(item,float):
            answer.append(item)
        else:
            second = answer.pop()
            first = answer.pop()
            result = operators[item]['action'](first,second)
            answer.append(result)

    return answer[0]


# invoking function
def calculate(expression):
    pieces = generate_token(expression)
    rpn = convert_to_RPN(pieces)
    answer = count_RPN(rpn)
    return answer


# running block 
if __name__ == "__main__":
    while True:
        expression = raw_input('Enter your string to calculate: \n');
        if expression.strip() == 'exit()':
            exit_calculator()
        print calculate(expression)