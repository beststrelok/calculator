##################################
###		  Calculator v1.4 	   ###
### on Reverse Polish Notation ###
##################################

#!/usr/bin/env python
from sys import exit as exit_calculator
from math import floor as math_floor


def divide_expression(expression):
	string = expression.replace(' ','')
	pieces = []
	chunk = ''
	signs = '^/*-+'
	
	# divide string into pieces
	for char in string:
		if char not in signs:
			chunk += char
		else:
			pieces.append(float(chunk))
			pieces.append(char)
			chunk = ''
	pieces.append(float(chunk))

	return pieces

def convert_to_RPN(pieces):
	out = []
	stack = []
	# Priority Levels: 
	# ^    high = 3	 
	# * /  average = 2 
	# + -  low = 1  
	priority = {
		'^' : 3,
		'/' : 2,
		'*' : 2,
		'-' : 1,
		'+' : 1,
	}

	#shunting-yard algorithm
	for piece in pieces:
		if isinstance(piece,float):
			out.append(piece)
		else:
			op1 = piece
			if len(stack) > 0:
				op2 = stack[-1]
				if priority[op1] <= priority[op2]:
					op2 = stack.pop()
					out.append(op2)
			stack.append(op1)

	return out + stack[::-1]

def count_RPN(rpn):
	answer = []

	# calculation on rpn
	for item in rpn:
		if isinstance(item,float):
			answer.append(item)
		else:
			second = answer.pop()
			first = answer.pop()
			if item == '^':
				result = first ** second
			elif item == '/':
				result = first / second
			elif item == '*':
				result = first * second
			elif item == '-':
				result = first - second
			elif item == '+':
				result = first + second

			answer.append(result)

	return answer[0]

# running block 
if __name__ == "__main__":
	expression = raw_input('Enter your string to calculate: \n');
	if expression.strip() == 'exit()':
		exit_calculator()
	pieces = divide_expression(expression)
	print pieces
	rpn = convert_to_RPN(pieces)
	print rpn

	answer = count_RPN(rpn)
	if answer - math_floor(answer) == 0:
		answer = int(answer)
	print answer