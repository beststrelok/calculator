##################################
###		  Calculator v1.1 	   ###
### on Reverse Polish Notation ###
##################################

#!/usr/bin/env python
from sys import exit as exit_calculator
from re import search as re_search 
from math import floor as math_floor

def convert_to_RPN(expression):
	### remove all spaces ###
	income_string = expression.replace(' ','')
	
	out = []
	stack = []
	RPN = []
	signs = '^/*-+'
	########################
	### Priority Levels  ###
	### ^    high = 3	 ###
	### * /  average = 2 ###
	### + -  low = 1     ###
	######################## 
	priority = {
		'^' : '3',
		'/' : '2',
		'*' : '2',
		'-' : '1',
		'+' : '1',
	}

	def iterate_step(income_string):
		### make a copy ###
		string = income_string
		count = 0

		### iterate through each charecter in string ###
		for char in string:
			if char in signs:
				### get index of first sign1 ###
				index = string.find(char)
				### get number from start to sign1 ###
				number = float(string[:index])
				### add this number to list "out" ###
				out.append(number)
				### get the sign1 character ###
				sign = string[index]

				### when there were signs in list 'out' ###
				### check for priority of signs else    ###
				### just add this sign1 to list 'stack'  ###
				if len(stack) >  0:
					### get top sign2 from list 'stack' ###
					last = stack.pop()
					### add sign1 to list 'stack' ###
					stack.append(sign)
					### check priority ###
					if priority[sign] <= priority[last]:
						out.append(last)
					else:
						stack.append(last)
				else:
					stack.append(sign)
				###########################################

				### remove token number and sign1 from string ###
				string = string.replace(string[:index+1],'',1)

		### check if there is any sign in string ###
		search = re_search('(\^|\*|\/|\-|\+)', string)
		if search == None:
			### if no else sign then make number from left ###
			number = float(string)
			out.append(number)
			### clear string ###
			string = ''

		if len(string) > 0:
			# iterate_step(string)
			# run recursively in latent way
			pass
		else: 
			return out + stack

	return iterate_step(income_string)

### realisation of Shunting-yard algorithm for RPN ###
def count_RPN(rpn):
	answer = []
	### iterate through each item in list ###
	for item in rpn:
		if isinstance(item,float):
			answer.append(item)
		else:
			second = answer.pop()
			first = answer.pop()
			if item == '^':
				result = first ** second
			elif item == '/':
				result = first / second
			elif item == '*':
				result = first * second
			elif item == '-':
				result = first - second
			elif item == '+':
				result = first + second

			answer.append(result)

	return answer[0]


#################
### Run Block ###
#################
if __name__ == "__main__":
	expression = raw_input('Enter your string to calculate: \n');
	if expression.strip() == 'exit()':
		exit_calculator()
	rpn = convert_to_RPN(expression)
	print rpn

	answer = count_RPN(rpn)
	if answer - math_floor(answer) == 0:
		answer = int(answer)
	print answer
#################