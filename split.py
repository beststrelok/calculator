#!/usr/bin/env python
import re

signs = '+-*/'

def split(string):
	array = []
	chunk = ''
	for char in string:
		if char not in signs:
			chunk += char
		else:
			array.append(chunk)
			chunk = ''

	array.append(chunk)
	return array
			 
if __name__ == "__main__":
	income_string = raw_input('Dividers: +, -, *, /. Enter your string:\n')
	array = split(income_string)
	print '\n'.join(array)